=============================
Django Google Tag Manager
=============================

.. image:: https://badge.fury.io/py/django-google-tag-manager.png
    :target: https://badge.fury.io/py/django-google-tag-manager

.. image:: https://travis-ci.org/yoraoul/django-google-tag-manager.png?branch=master
    :target: https://travis-ci.org/yoraoul/django-google-tag-manager

With this 

Documentation
-------------

The full documentation is at https://django-google-tag-manager.readthedocs.io.

Quickstart
----------

Install Django Google Tag Manager::

    pip install django-google-tag-manager

Add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'django_google_tag_manager',
        ...
    )

Add Django Google Tag Manager's URL patterns:

.. code-block:: python

    from django_google_tag_manager import urls as django_google_tag_manager_urls


    urlpatterns = [
        ...
        url(r'^', include(django_google_tag_manager_urls)),
        ...
    ]

Features
--------

* TODO

Running Tests
-------------

Does the code actually work?

::

    source <YOURVIRTUALENV>/bin/activate
    (myenv) $ pip install tox
    (myenv) $ tox

Credits
-------

Tools used in rendering this package:

*  Cookiecutter_
*  `cookiecutter-djangopackage`_

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`cookiecutter-djangopackage`: https://github.com/pydanny/cookiecutter-djangopackage

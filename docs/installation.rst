============
Installation
============

At the command line::

    $ easy_install django-google-tag-manager

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv django-google-tag-manager
    $ pip install django-google-tag-manager

=====
Usage
=====

To use Django Google Tag Manager in a project, add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'django_google_tag_manager',
        ...
    )

Add Django Google Tag Manager's URL patterns:

.. code-block:: python

    from django_google_tag_manager import urls as django_google_tag_manager_urls


    urlpatterns = [
        ...
        url(r'^', include(django_google_tag_manager_urls)),
        ...
    ]
